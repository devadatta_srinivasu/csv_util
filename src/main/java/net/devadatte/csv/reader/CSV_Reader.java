package net.devadatte.csv.reader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import net.devadatta.util.InitCap;

public class CSV_Reader {
	public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		String csvFile = "/home/srinivasu/Desktop/test.csv";
		BufferedReader br = null;
		String line = "";
		String csvSplitBy = ",";
		String csvHeaders[] = new String[0];
		try {
			 
			br = new BufferedReader(new FileReader(csvFile));
			//reading the first line
			if((line = br.readLine()) != null){
				csvHeaders = line.split(csvSplitBy);
			}
			while ((line = br.readLine()) != null) {
				String[] data = line.split(csvSplitBy);
				if(data.length == csvHeaders.length){
					Class class1 = Class.forName("net.devadatte.csv.reader.Student");
					System.out.println(class1.getName());
					System.out.println(class1.getSimpleName());
					Constructor constructor = class1.getConstructor();
					Student student =  (Student) constructor.newInstance();
					for (int i = 0; i < csvHeaders.length; i++) {
						String csvHeader = csvHeaders[i];
						Method method = class1.getMethod("set"+InitCap.initCap(csvHeader), new Class[]{String.class});
						System.out.println(method.getName());
						method.invoke(student, data[i]);
					}
					System.out.println(student);
				}
			    
			}
	 
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	 
		System.out.println("Done");
	}
}

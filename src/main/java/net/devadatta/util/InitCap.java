package net.devadatta.util;

public class InitCap {
	public static String initCap(String name){
		if(name == null){
			return null;
		}else if(name.length() == 1 ){
			return name.toUpperCase();
		}else{
			name=name.substring(0,1).toUpperCase()+name.substring(1, name.length()).toLowerCase();
			return name;
		}
	}
}
